import {
  Heading,
  Box,
  Grid,
  HStack,
  IconButton,
  Link,
  BoxProps,
  useColorModeValue,
} from "@chakra-ui/react"
import {FaGitlab} from "react-icons/fa"

import {ColorModeSwitcher} from "@/components"

interface HeaderProps extends BoxProps {}

const Header = ({...props}: HeaderProps) => {
  return (
    <Box
      bg={useColorModeValue("white", "gray.700")}
      w="100%"
      borderBottom="solid"
      borderColor={useColorModeValue("black", "white")}
      py="4"
      position="sticky"
      top={0}
      zIndex={1}
      {...props}
    >
      <Grid templateColumns="1fr auto 1fr" justifyItems="center">
        <Heading textAlign="center" gridColumnStart="2">
          🦄 Everyday Billionaire
        </Heading>
        <HStack ml="auto" pr={4}>
          <Link href="https://gitlab.com/Jpac14/everyday-billionaire" target="_blank">
            <IconButton icon={<FaGitlab />} aria-label="Gitlab" />
          </Link>
          <ColorModeSwitcher />
        </HStack>
      </Grid>
    </Box>
  )
}

export type {HeaderProps}
export {Header}

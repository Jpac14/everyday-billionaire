import useStore from "@/store"

import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalCloseButton,
  ModalFooter,
  IconButton,
  Button,
  VStack,
  HStack,
  Input,
  IconButtonProps,
  useClipboard,
} from "@chakra-ui/react"

import {
  EmailShareButton,
  FacebookShareButton,
  TwitterShareButton,
  LinkedinShareButton,
  TelegramShareButton,
  WhatsappShareButton,
  RedditShareButton,
} from "react-share"

import {
  FaEnvelope,
  FaFacebook,
  FaTwitter,
  FaLinkedin,
  FaTelegram,
  FaWhatsapp,
  FaReddit,
} from "react-icons/fa"

const ShareButton = ({...props}: IconButtonProps) => {
  return <IconButton {...props} />
}

interface ShareModalProps {}

const ShareModal = ({...props}: ShareModalProps) => {
  const [shareModalOpen, closeShareModal, shareIdea] = useStore((state) => [
    state.shareModalOpen,
    state.closeShareModal,
    state.shareIdea,
  ])

  const {title, fullUrl} = shareIdea ?? {
    id: "",
    title: "",
    description: "",
    fullUrl: "",
    addedOn: "",
  }

  const {hasCopied, onCopy} = useClipboard(fullUrl)

  return (
    <Modal isOpen={shareModalOpen} onClose={closeShareModal} isCentered>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Sharing "{title}"</ModalHeader>
        <ModalCloseButton />
        <ModalBody>
          <VStack>
            <HStack>
              <EmailShareButton url={fullUrl}>
                <ShareButton icon={<FaEnvelope />} aria-label="Share via Email" />
              </EmailShareButton>
              <FacebookShareButton url={fullUrl}>
                <ShareButton
                  icon={<FaFacebook />}
                  aria-label="Share via Facebook"
                  colorScheme="facebook"
                />
              </FacebookShareButton>
              <TwitterShareButton url={fullUrl}>
                <ShareButton
                  icon={<FaTwitter />}
                  aria-label="Share via Twitter"
                  colorScheme="twitter"
                />
              </TwitterShareButton>
              <LinkedinShareButton url={fullUrl}>
                <ShareButton
                  icon={<FaLinkedin />}
                  aria-label="Share via Linkedin"
                  colorScheme="linkedin"
                />
              </LinkedinShareButton>
              <TelegramShareButton url={fullUrl}>
                <ShareButton
                  icon={<FaTelegram />}
                  aria-label="Share via Telegram"
                  colorScheme="telegram"
                />
              </TelegramShareButton>
              <WhatsappShareButton url={fullUrl}>
                <ShareButton
                  icon={<FaWhatsapp />}
                  aria-label="Share via Whatsapp"
                  colorScheme="whatsapp"
                />
              </WhatsappShareButton>
              <RedditShareButton url={fullUrl}>
                <ShareButton icon={<FaReddit />} aria-label="Share via Reddit" colorScheme="red" />
              </RedditShareButton>
            </HStack>
            <HStack>
              <Input value={fullUrl} isDisabled />
              <Button onClick={onCopy} colorScheme={hasCopied ? "green" : "blue"} w="20">
                {hasCopied ? "Copied" : "Copy"}
              </Button>
            </HStack>
          </VStack>
        </ModalBody>
        <ModalFooter />
      </ModalContent>
    </Modal>
  )
}

export type {ShareModalProps}
export {ShareModal}

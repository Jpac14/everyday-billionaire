import useStore from "@/store"

import {
  Heading,
  Text,
  Center,
  Link,
  VStack,
  Box,
  IconButton,
  useColorModeValue,
} from "@chakra-ui/react"
import {FaShare} from "react-icons/fa"
import {motion} from "framer-motion"
import {BackdropContainer} from "@/components"

import {Idea} from "@/models"

import {DateTime} from "luxon"

interface IdeaCardProps {
  idea: Idea
  index: number
}

const IdeaCard = ({idea, index}: IdeaCardProps) => {
  const {title, description, fullUrl, addedOn} = idea
  const [setShareIdea, openShareModal] = useStore((state) => [state.setShareIdea, state.openShareModal])

  const onShare = async () => {
    setShareIdea(idea)

    if (navigator.share) { 
      try {
        await navigator.share({
          title,
          url: fullUrl,
        })
      } catch (error) {
        console.error(error)
      }
    } else {
      openShareModal()
      console.warn("Web share is not supported!")
    }
  }

  return (
    <motion.div initial={{opacity: 0}} animate={{opacity: 1}}>
      <BackdropContainer
        bg={useColorModeValue("white", "gray.700")}
        borderRadius="md"
        boxShadow="md"
        w="72"
        h="48"
        border="solid"
        borderColor={useColorModeValue("black", "white")}
        index={index}
      >
        <Text position="absolute" p="2" fontSize="xs">
          {DateTime.fromISO(addedOn).toFormat("d MMM yy")}
        </Text>
        <Center h="full" p="4">
          <VStack>
            <Link href={fullUrl} target="_blank">
              <Heading textAlign="center" fontSize="24">
                {title}
              </Heading>
            </Link>
            <Text textAlign="center" fontSize="sm">
              {description}
            </Text>
          </VStack>
        </Center>
        <Box position="absolute" bottom={0} right={0} p={1}>
          <IconButton
            icon={<FaShare />}
            aria-label="Share Idea"
            variant="ghost"
            size="sm"
            onClick={onShare}
          />
        </Box>
      </BackdropContainer>
    </motion.div>
  )
}

export type {IdeaCardProps}
export {IdeaCard}

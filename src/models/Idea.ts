interface Idea {
  id: string
  title: string
  description: string
  fullUrl: string
  addedOn: string // ISO formatted
}

export type {Idea}
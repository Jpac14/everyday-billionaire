import {Idea} from "@/models"

interface IdeaPage {
  nextPage: boolean
  nextPageUrl: string
  ideas: Idea[]
}

export type {IdeaPage}
import {useEffect} from "react"
import type {NextPage} from "next"

import useSWRInfinite from "swr/infinite"
import {fetcher, getKey} from "@/data"

import {Center, SimpleGrid, Heading, HStack, Spinner} from "@chakra-ui/react"
import {WarningIcon} from "@chakra-ui/icons"
import {IdeaCard, Header, ShareModal} from "@/components"

const Index: NextPage = () => {
  const {data, error, size, setSize} = useSWRInfinite(getKey, fetcher)

  const isLoadingInitialData = !data && !error
  const isLoadingMore =
    isLoadingInitialData || (size > 0 && data && typeof data[size - 1] === "undefined")

  const handleScroll = () => {
    const bottom =
      Math.ceil(window.innerHeight + window.scrollY) + 300 >= document.documentElement.scrollHeight
    if (bottom && !isLoadingMore) setSize(size + 1)
  }

  useEffect(() => {
    window.addEventListener("scroll", handleScroll, {
      passive: true,
    })

    return () => {
      window.removeEventListener("scroll", handleScroll)
    }
  }, [handleScroll])

  if (!data)
    return (
      <Center h="100vh">
        <HStack>
          <Heading>Loading...</Heading>
          <Spinner thickness="4px" speed="0.65s" emptyColor="gray.200" color="blue.500" size="lg" />
        </HStack>
      </Center>
    )

  if (error)
    return (
      <Center h="100vh">
        <HStack>
          <Heading>Error</Heading>
          <WarningIcon color="red.500" boxSize="8" />
        </HStack>
      </Center>
    )

  return (
    <>
      <Header />
      <Center p="8">
        <SimpleGrid columns={{sm: 1, lg: 3}} spacing="16">
          {data.map((page) => {
            return page.ideas.map((idea, index) => (
              <IdeaCard idea={idea} index={index} key={idea.id} />
            ))
          })}
        </SimpleGrid>
      </Center>
      <ShareModal />
    </>
  )
}

export default Index

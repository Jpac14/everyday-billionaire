import type {AppProps} from "next/app"
import Head from "next/head"

import {ChakraProvider, extendTheme} from "@chakra-ui/react"
import "@fontsource/inter"

const theme = extendTheme({
  fonts: {
    body: "Inter, system-ui, sans-serif",
    heading: "Inter, system-ui, sans-serif",
  },
})

function App({Component, pageProps}: AppProps) {
  return (
    <>
      <Head>
        <title>Everyday Billionaire</title>

        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
      </Head>
      <ChakraProvider theme={theme}>
        <Component {...pageProps} />
      </ChakraProvider>
    </>
  )
}

export default App

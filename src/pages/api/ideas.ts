import type {NextApiRequest, NextApiResponse} from "next"

import {IdeaPage} from "@/models"

const BASE_URL = "https://www.billiondollarstartupideas.com"

export default async (req: NextApiRequest, res: NextApiResponse<IdeaPage>) => {
  const {offset} = req.query as {offset: string}
  const url = `${BASE_URL}/ideas?format=json${offset ? `&offset=${offset}` : ""}`

  const data = await (await fetch(url)).json()
  
  res.json({
    nextPage: data.pagination.nextPage,
    nextPageUrl: `/api/ideas?offset=${data.pagination.nextPageOffset}`,
    ideas: data.items.map((item: any) => ({
      id: item.id,
      title: item.title,
      description: item.excerpt.replace(/<\/?[^>]+(>|$)/g, ""),
      fullUrl: BASE_URL + item.fullUrl,
      addedOn: new Date(item.addedOn).toISOString(),
    })),
  })
}

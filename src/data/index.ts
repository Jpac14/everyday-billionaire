import {Fetcher} from "swr"
import {SWRInfiniteKeyLoader} from "swr/infinite"

import {IdeaPage} from "@/models"

const fetcher: Fetcher<IdeaPage, string> = async (url) => {
  const res = await fetch(url)
  return await res.json()
}

const getKey: SWRInfiniteKeyLoader = (index, previousPageData: IdeaPage) => {
  if (previousPageData && !previousPageData.nextPage) return null
  if (index === 0) return "/api/ideas"
  return previousPageData.nextPageUrl
}

export {fetcher, getKey}
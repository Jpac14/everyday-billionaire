import create from "zustand"

import {Idea} from "@/models"

interface Store {
  shareModalOpen: boolean
  openShareModal: () => void
  closeShareModal: () => void
  toggleShareModal: () => void

  shareIdea: Idea | undefined
  setShareIdea: (idea: Idea) => void
}

const useStore = create<Store>((set) => ({
  shareModalOpen: false,
  openShareModal: () => set(() => ({shareModalOpen: true})),
  closeShareModal: () => set(() => ({shareModalOpen: false})),
  toggleShareModal: () => set((state) => ({shareModalOpen: !state.shareModalOpen})),

  shareIdea: undefined,
  setShareIdea: (idea: Idea) => set((state) => ({shareIdea: idea})),
}))

export default useStore
